---
title: Privacy is about taking informed decisions
published: true
description: Do you trust each service you use? Do you really need each piece of content you consume? That's what I started asking myself and it is paying back.
tags: privacy
series: Privacy myths
---

My [digital detox](https://en.wikipedia.org/wiki/Digital_detox) is a journey that has just started for me on reducing or changing some activities I do online that are cause more harm than good. So, it's more of a diet than a detox.

## Social media

Intervention: Understand why I use each social media
Convenience: I keep linkedin for professional networking. I like twitter but I have to structure it somehow. Who I'm following and why (like adding them to lists, etc). Also, I'm more of an introvert but that's an opportunity to change right? As regards facebook, I login something like ones every 6 months so I might delete it, let's see. For now, I just delete linkedin and twitter from my android so that I have to login explicitly without being annoyed by push notifications and with a bit less tracking. Some useful Twitter settings are: [photo tagging](https://twitter.com/settings/tagging), [personalize based on your inferred identity](https://twitter.com/settings/off_twitter_activity), [location](https://twitter.com/settings/location_information)

Most importantly, whenever I login in twitter/linkedin, I remember to logout so that it doesn't track me on other sites.

### Browsing

[Issues](https://edit.tosdr.org/points/12825):

1. This service tracks you on other websites. When you visit sites that use Google Analytics, Google and a Google Analytics customer may link information about your activity from that site with activity from other sites that use our ad services.</p>

Intervention: Replaced chrome with vivaldi
Convenience: Vivaldi uses chromium and so far it works. I started with firefox but for example, gitlab couldn't load the login page...

1. Install chrome extension Privacy Badger
2. At advanced settings, enable "Learn to block new trackers from your browsing"
3. Explicitly block the [top 10 trackers](https://webtransparency.cs.princeton.edu/webcensus/) because [you should have been asked first](https://vivaldi.com/blog/how-to-stop-online-tracking/). This doesn't mean that you won't be able to browse www.youtube.com for example.

At tracking domains, find the following trackers of [category advertising](https://whotracks.me/trackers.html) and block them.

google
doubleclick
facebook
youtube
googlesyndication
googleadservices
twitter
reddit

## Android settings

Intervention: A number of settings in the phone and online
Convenience: 100%. By the way, writing the convenience level like [here](https://www.computerworld.com/article/3545530/ultimate-guide-to-privacy-on-android.html) is great for digital detox guides.

1. Turn off Google's ad personalization system
2. Disable Android's location history feature
3. Disable your Google Web & App Activity

## Google search

Intervention: Replace it with ecosia.com and use "g ..." when searching something technical like a programming error
Convenience: I was afraid it would be slow, give zero results or irrelevant results but so far it's great! Initially I was using qwant.com but they disabled it for days in several countries without any prior notifications so their service looks a bit shaddy.

[Some issues](https://tosdr.org/#ecosia): 

1. We use the locational data in order to give you a better Ecosia experience or to show more relevant search results **or advertising to you.**
2. Ecosia allows third-party providers to set cookies in order to check the success of Ecosia advertising campaigns.

## Youtube

[Some issues](https://tosdr.org/#youtube):

1. The activity information we collect may include: [...] Chrome browsing history you’ve synced with your Google Account</li>

Intervention: Replace it with Vimeo/Dailymotion
Convenience: I'll have to create first a local library of music for me and videos for my kids. This way I'll get rid of the ads too.

The Vimeo tracker seams to be for [functionality](https://whotracks.me/trackers/dailymotion_advertising.html) whereas the Dailymotion tracker seams to be for [advertising](https://whotracks.me/trackers/dailymotion_advertising.html)

## Mobile email client

Intervention: Replace gmail with sparkmail
Convenience: After learning a few settings of sparkmail, it was great to have less notifications. Steps:

1. Remove all my gmail accounts apart from the main one (which is for my android)
2. Add them to sparkmail
3. Uninstall gmail which just resets it to a gmail app with no account
4. At settings of the main one, turn off notifications 
5. At sparkmail, enable smart notification and set to personal the few critical email contacts I have. So far all non critical contacts like newsltters are detected automatically and I have set it to receive no notifications.

## Gmail

Intervention: Replace gmail with fastmail
Convenience: I created one but this might take months as I'll have to evaluate calendar, contacts and Files (it has something like google drive), then move each service and newsletter to this one. At [fastmail](www.fastmail.com/settings/preferences), "always ask before showing remote images" to avoid [pixel tracking](https://www.digitalmarketer.com/blog/what-is-tracking-pixel).

## Google maps

Intervention: Replace google maps with [HERE](https://wego.here.com)
Convenience: Probably the lowest here as it might not work at all. I installed it but I haven't tried it yet.

## TODO

Intervention: Replace google docs with notion and google sheets with airtable
Convenience: I'll have to get used to that but I'm confident and I don't have many google docs anyway.

Intervention: Use a VPN like NordVPN to hide my IP
Convenience: Probably the lowest here too as some websites might be slow. Also, I'm not sure if there any value in Onion sites but I'm keeping it in my list.

Follow me at dev.to if you want more posts with the tag "privacy".

Are you in a digital detox too?
