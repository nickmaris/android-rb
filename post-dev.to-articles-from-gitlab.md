---
title: Post dev.to articles from gitlab
published: true
description: Markdown and gitlab make it so easy to talk about changes
tags: gitlab
---

Whether you review your draft dev.to articles alone or with friends, markdown and gitlab make it very easy to talk about changes, so here is a way to write dev.to articles on gitlab and schedule their publishing.

{% twitter 1326907703634096129 %}

{% twitter 1327198242346573826 %}

## Instructions

1. fork [this](https://gitlab.com/nickmaris/android-rb) gitlab repository
2. at dev.to [generate token](https://docs.dev.to/api/#section/Authentication/api_key)
2. at gitlab set masked CI env vars DEV_TO_API_KEY, DEV_TO_NAME
3. at gitlab set personal access token with write repository permissions
4. at gitlab set CI env vars masked GITLAB_TOKEN
5. at gitlab set a scheduled pipeline on a daily basis

## How it works

Gitlab CI processes entries in state.yml of master branch daily like the following:

```
- filename: update.md
  date: 2020-11-08
  slug: generated-post-19p6

```

If today is `2020-11-08` then it updates the article with title mentioned in `update.md`.

And if the article does not exist, it creates it and updates `state.yml` with its slug so that it can update it in the future if instructed by `state.yml`.

Internally, the whole logic of the [ruby script](https://gitlab.com/nickmaris/android-rb/-/blob/master/dev_to_article.rb) is about putting the markdown on the body_markdown param of the dev.to API.

If `GET articles/#{username}/#{slug}` returns HTTP 200, then `PUT articles/#{id}` otherwise `POST articles`.

## The editing pipeline as a...pipeline

Each time you want to write a new article, open a Merge Request, create an entry at `state.yml` and a file.

Then the most important step is to discuss it with others and review it in an MR like [this](https://gitlab.com/nickmaris/android-rb/-/merge_requests/4)

Making the editing pipeline explicit in gitlab also opens the road to storing metrics (like number of comments) on state.yml and to further automation like daily checking for [broken links](https://github.com/tcort/markdown-link-check), correcting [spelling and grammar mistakes](https://github.com/subosito/gingerice), cross-posting, etc.

When ready, merge the MR. The next day in the scheduled timing it will appear at dev.to.

## Format

Custom variables can be set for each post, located between the triple-dashed lines in your editor. Make sure `published` is true and that there is a unique title:

* title: the title of your article
* published: boolean that determines whether or not your article is published
* description: description area in Twitter cards and open graph cards
* tags: max of four tags, needs to be comma-separated
* canonical_url: link for the canonical version of the content
* cover_image: cover image for post, accepts a URL. The best size is 1000 x 420.
* series: post series name.

## Next step

To edit this [post](https://gitlab.com/nickmaris/android-rb/-/blob/master/post-dev.to-articles-from-gitlab.md), the [ruby script](https://gitlab.com/nickmaris/android-rb/-/blob/master/dev_to_article.rb) or its [tests](https://gitlab.com/nickmaris/android-rb/-/blob/master/dev_to_article_spec.rb), feel free to [open an issue](https://gitlab.com/nickmaris/android-rb/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
