---
title: Gitlab settings for emails
published: true
description: Set up functionality of gitlab around emails
tags: gitlab
---

One of the first thing I did after installing gitlab was to set up functionality around emails. Here is how I did that.

For SMTP, instead of rushing to use the [less secure apps](https://myaccount.google.com/lesssecureapps) option of gmail, you can do the following:
1. [enable two factor authentication](myaccount.google.com/u/1/signinoptions/two-step-verification)
2. generate an [app password](https://support.google.com/accounts/answer/185833?hl=en) limited to email (and not e.g. youtube)

You need to replace also the sensitive data below that is in env vars with your own data.

## Send to gitlab

Sending emails helps to reply in a comment in issue or MR or open an issue. The last one is feasible for free since Service Desk moved to core in version [13.2](https://docs.gitlab.com/ee/administration/incoming_email.html#set-it-up)

/etc/gitlab/gitlab.rb

```
gitlab_rails['smtp_enable'] = true 
gitlab_rails['smtp_address'] = "smtp.gmail.com"
gitlab_rails['smtp_port'] = 587 
gitlab_rails['smtp_user_name'] = $SMTP_USER_NAME
gitlab_rails['smtp_password'] = $SMTP_PASSWORD
gitlab_rails['smtp_domain'] = "gmail.com" 
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = false 
gitlab_rails['smtp_openssl_verify_mode'] = 'peer'
```

and run `sudo gitlab-ctl reconfigure`

## Receive

Be able to receive emails from gitlab like a job status update.

/etc/gitlab/gitlab.rb

```
gitlab_rails['gitlab_email_enabled'] = true
gitlab_rails['gitlab_email_from'] = 'gitlab@tolen.ddns.net'
gitlab_rails['gitlab_email_display_name'] = 'Gitlab - Admin'
gitlab_rails['gitlab_email_reply_to'] = 'gitlab@tolen.ddns.net'
gitlab_rails['gitlab_email_subject_suffix'] = ''
```

smime

## fulldiskspace

sudo ufw allow 9093/tcp

/et/gitlab/gitlab.rb

```
alertmanager['enable'] = true alertmanager['admin_email'] = 'xxxxxxxxxx' alertmanager['listen_address'] = '0.0.0.0:9093'
```