require_relative 'dev_to_article'
require_relative 'spec_helper'

describe DevToArticle do
  context 'you are user u with api key k,' do
    username = 'u'
    apikey = 'k'
    slug = 'p'
    id = '12345'
    md = "---\n"
    md += "title: Sample Article\n"
    md += "published: \n"
    md += "description: this is a sample \n"
    md += "tags: test\n"
    md += "series: a series\n"
    md += "---\n"
    md += "\n"
    md += "...\n"
    context 'dev.to has article with the same slug,' do
      before do
        id = 12345
        stub_request(:any, /dev.to/).
          to_return(
            status: 200,
            headers: {"Content-Type": "application/json"},
            body: { 'id': id }.to_json
          )
      end
      it 'updates article' do
        DevToArticle.upsert(apikey, username, slug, md)
  
        expect(
          a_request(:put, /dev.to\/api\/articles\/#{id}/).
          with(body: hash_including({
            article: {
              body_markdown: md
            }
          }))
        ).to have_been_made.once
      end
    end
    context 'dev.to has no article with this slug' do
      before do
        stub_request(:post, /dev.to/).
          to_return(
            status: 200,
            headers: {"Content-Type": "application/json"},
            body: { 'slug': 'q' }.to_json
          )
        stub_request(:get, /dev.to/).
          to_return(status: 404)
      end
      it 'inserts new article' do
        DevToArticle.upsert(apikey, username, slug, md)
        
        expect(
          a_request(:post, /dev.to\/api\/articles/).
          with(body: hash_including({
            article: {
              body_markdown: md
            }
          }))
        ).to have_been_made.once
      end
      it 'gets slug for future updates' do
        slug = DevToArticle.upsert(apikey, username, slug, md)
        
        expect(slug).to eq('q')
      end
    end
   end
 end