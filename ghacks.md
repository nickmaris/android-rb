---
title: Ghacks - A service with a structured privacy popup
published: true
description: Having a structure helps you read a privacy popup faster
tags: privacy
series: Privacy myths
---

A Consent and Preference Management Platform like didomi.io shows the popup that you press Agree without reading anything in most websites you ever visit like ghacks.net. So most people, including myself, are trading privacy with convenience. It looks similar to installing software in Windows, just press "Yes to All" to accept the defaults. After all, what can go wrong, right? 

Privacy popups are asking you to take an informed decision. Some have no information, some have information that you need to pay a lawyer to understand it and some like ghacks.net have it in a pretty structured way, so let's read how it looked on the 2nd of December 2020:

## An informative privacy popup

Softonic uses own and third-party cookies to show personalized content and ads on Softonic’s properties and other sites, to provide social media features, to create an anonymous profile based on browsing behavior (visited pages, frequency, time) and to analyze how our services are used.
By clicking or scrolling on the site, you accept their use. You can decide what type of cookies you accept or withdraw the consent at any time:
With your agreement, we and our partners use cookies or similar technologies to store, access, and process personal data like your visit on this website. You can withdraw your consent or object to data processing based on legitimate interest at any time by clicking on "Learn More" or in our Privacy Policy on this website. 

> partners is a word with a link to the form described in the last section of this page

We and our partners do the following data processing based on your consent and/or our legitimate interest:

> this sentence is almost hidden

Personalised ads and content, ad and content measurement, audience insights and product development, Precise geolocation data, and identification through device scanning, Store and/or access information on a device

> this sentence is slightly more visible

## Learn more

Welcome to Ghacks!

We and our partners place cookies, access and use non-sensitive information from your device to improve our products and personalize ads and other contents throughout this website. You may accept all or part of these operations. To learn more about cookies, partners, and how we use your data, to review your options or these operations for each partner, visit our privacy policy. **The link is broken**

### Store and/or access information on a device (Disagree/Agree)

Cookies, device identifiers, or other information can be stored or accessed on your device for the purposes presented to you.

Vendors can:
* Store and access information on the device such as cookies and device identifiers presented to a user.

### Develop and improve products

Your data can be used to improve existing systems and software, and to develop new products

To develop new products and improve products vendors can:
* Use information to improve their existing products with new features and to develop new products
* Create new models and algorithms through machine learning
Vendors cannot:
* Conduct any other data processing operation allowed under a different purpose under this purpose

### Apply market research to generate audience insights

Market research can be used to learn more about the audiences who visit sites/apps and view ads.

To apply market research to generate audience insights vendors can:
* Provide aggregate reporting to advertisers or their representatives about the audiences reached by their ads, through panel-based and similarly derived insights.
* Provide aggregate reporting to publishers about the audiences that were served or interacted with content and/or ads on their property by applying panel-based and similarly derived insights.
* Associate offline data with an online user for the purposes of market research to generate audience insights if vendors have declared to match and combine offline data sources (Feature 1)
* Combine this information with other information previously collected including from across websites and apps. 
Vendors cannot:
* Measure the performance and effectiveness of ads that a specific user was served or interacted with, without a Legal Basis to measure ad performance.
* Measure which content a specific user was served and how they interacted with it, without a Legal Basis to measure content performance.

### Measure content performance

The performance and effectiveness of content that you see or interact with can be measured.

To measure content performance vendors can:
* Measure and report on how content was delivered to and interacted with by users.
* Provide reporting, using directly measurable or known information, about users who interacted with the content
* Combine this information with other information previously collected, including from across websites and apps.
Vendors cannot:
* Measure whether and how ads (including native ads) were delivered to and interacted with by a user.
* Apply panel- or similarly derived audience insights data to ad measurement data without a Legal Basis to apply market research to generate audience insights (Purpose 9)

### Measure ad performance

The performance and effectiveness of ads that you see or interact with can be measured.

To measure ad performance vendors can:
* Measure whether and how ads were delivered to and interacted with by a user
* Provide reporting about ads including their effectiveness and performance
* Provide reporting about users who interacted with ads using data observed during the course of the user's interaction with that ad
* Provide reporting to publishers about the ads displayed on their property
* Measure whether an ad is serving in a suitable editorial environment (brand-safe) context
* Determine the percentage of the ad that had the opportunity to be seen and the duration of that opportunity
* Combine this information with other information previously collected, including from across websites and apps
Vendors cannot:
*Apply panel- or similarly-derived audience insights data to ad measurement data without a Legal Basis to apply market research to generate audience insights (Purpose 9)

### Select personalised content

Personalised content can be shown to you based on a profile about you.

To select personalised content vendors can:
* Select personalised content based on a user profile or other historical user data, including a user’s prior activity, interests, visits to sites or apps, location, or demographic information.

### Create a personalised content profile

A profile can be built about you and your interests to show you personalised content that is relevant to you.

To create a personalised content profile vendors can:
* Collect information about a user, including a user's activity, interests, visits to sites or apps, demographic information, or location, to create or edit a user profile for personalising content.
* Combine this information with other information previously collected, including from across websites and apps, to create or edit a user profile for use in personalising content.

### Select personalised ads

Personalised ads can be shown to you based on a profile about you.

To select personalised ads vendors can:
* Select personalised ads based on a user profile or other historical user data, including a user’s prior activity, interests, visits to sites or apps, location, or demographic information.

### Create a personalised ads profile

A profile can be built about you and your interests to show you personalised ads that are relevant to you.

To create a personalised ads profile vendors can:
* Collect information about a user, including a user's activity, interests, demographic information, or location, to create or edit a user profile for use in personalised advertising.
* Combine this information with other information previously collected, including from across websites and apps, to create or edit a user profile for use in personalised advertising.

### Select basic ads

Ads can be shown to you based on the content you’re viewing, the app you’re using, your approximate location, or your device type.

To do basic ad selection vendors can:
* Use real-time information about the context in which the ad will be shown, to show the ad, including information about the content and the device, such as: device type and capabilities, user agent, URL, IP address
* Use a user’s non-precise geolocation data
* Control the frequency of ads shown to a user.
* Sequence the order in which ads are shown to a user.
* Prevent an ad from serving in an unsuitable editorial (brand-unsafe) context
Vendors cannot:
* Create a personalised ads profile using this information for the selection of future ads without a separate legal basis to create a personalised ads profile.
* N.B. Non-precise means only an approximate location involving at least a radius of 500 meters is permitted.

### Actively scan device characteristics for identification

Your device can be identified based on a scan of your device's unique combination of characteristics.

Vendors can:
* Create an identifier using data collected via actively scanning a device for specific characteristics, e.g. installed fonts or screen resolution.
* Use such an identifier to re-identify a device.

### Use precise geolocation data

Your precise geolocation data can be used in support of one or more purposes. This means your location can be accurate to within several meters.

Vendors can:
* Collect and process precise geolocation data in support of one or more purposes.
N.B. Precise geolocation means that there are no restrictions on the precision of a user’s location; this can be accurate to within several meters.

By giving consent to the purposes above, you also allow this website and its partners to operate the following data processing:

### Ensure security, prevent fraud, and debug

Your data can be used to monitor for and prevent fraudulent activity, and ensure systems and processes work properly and securely. To ensure security, prevent fraud and debug vendors can:
* Ensure data are securely transmitted
* Detect and prevent malicious, fraudulent, invalid, or illegal activity.
* Ensure correct and efficient operation of systems and processes, including to monitor and enhance the performance of systems and processes engaged in permitted purposes
Vendors cannot:
* Conduct any other data processing operation allowed under a different purpose under this purpose.
Note: Data collected and used to ensure security, prevent fraud, and debug may include automatically-sent device characteristics for identification, precise geolocation data, and data obtained by actively scanning device characteristics for identification without separate disclosure and/or opt-in.

### Link different devices

Different devices can be determined as belonging to you or your household in support of one or more of purposes. Vendors can:
* Deterministically determine that two or more devices belong to the same user or household
* Probabilistically determine that two or more devices belong to the same user or household
* Actively scan device characteristics for identification for probabilistic identification if users have allowed vendors to actively scan device characteristics for identification (Special Feature 2)

### Match and combine offline data sources

Data from offline data sources can be combined with your online activity in support of one or more purposes Vendors can:
* Combine data obtained offline with data collected online in support of one or more Purposes or Special Purposes.

### Receive and use automatically-sent device characteristics for identification

Your device might be distinguished from other devices based on information it automatically sends, such as IP address or browser type. Vendors can:
* Create an identifier using data collected automatically from a device for specific characteristics, e.g. IP address, user-agent string.
* Use such an identifier to attempt to re-identify a device.
Vendors cannot:
* Create an identifier using data collected via actively scanning a device for specific characteristics, e.g. installed font or screen resolution without users’ separate opt-in to actively scanning device characteristics for identification.
* Use such an identifier to re-identify a device.

### Technically deliver ads or content

Your device can receive and send information that allows you to see and interact with ads and content. To deliver information and respond to technical requests vendors can:
* Use a user’s IP address to deliver an ad over the internet
* Respond to a user’s interaction with an ad by sending the user to a landing page
* Use a user’s IP address to deliver content over the internet
* Respond to a user’s interaction with content by sending the user to a landing page
* Use information about the device type and capabilities for delivering ads or content, for example, to deliver the right size ad creative or video file in a format supported by the device
Vendors cannot:
* Conduct any other data processing operation allowed under a different purpose under this purpose

### Select partners for Ghacks (Block/Authorize all of selected ones)

You can set your consent preferences for every partner listed below individually. Click on a partner name to get more information on what it does, what data it is collecting and how it is using it.

The form has a "User ID" and a "Didomi Token" and 643 partners! One of which is: Dailymotion SA

Data processing based on consent (Block/Authorize):

Create a personalised ads profile
Select personalised ads
Store and/or access information on a device

Data processing based on legitimate interest (Block/Authorize):
Create a personalised content profile
Develop and improve products
Measure ad performance
Measure content performance
Select basic ads
Select personalised content

Additional data processing:

Ensure security, prevent fraud, and debug
Link different devices
Technically deliver ads or content
Dailymotion SA participates in the [IAB Transparency and Consent Framework](https://iabeurope.eu/tcf-2-0/) and is subject to its policies. You can learn more about this partner and its data processing in its [privacy policy](https://www.dailymotion.com/legal/privacy).

## Conclusion

What is important is not to just have website owners showing verbose popups like didomi.io, it is to have visitors reading them careful and be aware of the choices they make and the impact of these choices on themselves. Ghacks is not bad, it actually has a great privacy popup. So we need to stop, read and think before we start consuming content from a source that is new to us and having a structure helps you read a privacy popup faster.

Personally, this time, I didn't press neither Disagree nor Agree, nor I went to any other site. I realized that I didn't really need that google search that landed me there and I closed the tab.

If you don't see the next article in this series called "Privacy myths" yet, follow me at dev.to and I'll post it tomorrow.
