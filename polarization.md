---
title: Privacy is not about Polarization
published: true
description: Even though polarization is an issue
tags: privacy
series: Privacy myths
---


Polarization in social media is an issue and parents and consultants of governmental policy makers need to take action but it is not privacy, so what is it?

The more attention we pay in distractions, the easier we make it to end up in a fight if we don't have the discipline to stop engaging into that yet. What is different in internet than say TV and newspaper is that internet knows you, personally. Facebook Site Integrity fake engagement team has described governments in Azerbaijan, Honduras, India, Ukraine and Bolivia using Facebook against their own citizens, employing large numbers of fake accounts to promote their own interests like elections and attack critics by buying inauthentic likes, comments, and shares. When an [issue](https://www.theverge.com/interface/2020/9/16/21437942/social-dilemma-netflix-review-orlowski-sarah-zhang-memo-facebook-buzzfeed) escalated, the company said that it was less pressing than the many other issues the civic integrity team was policing at the time, in other countries all over the world.

## What to do

To avoid polarization, here are a few steps that require discipline based on [humanetech](https://www.humanetech.com/take-control):

1. ‍Unfollow outrage-chasing individuals/groups
2. Remove sharply polarized media outlets from your news feed → [Humanetech](https://www.humanetech.com/take-control) mentions MSNBC & FOXNews
3. ‍Check news sites whose perspectives you disagree with: [AllSides](https://www.allsides.com/unbiased-balanced-news) gives readers a cross-partisan view of world events covered by the media, and sustains itself on a consciously created hybrid revenue model to avoid bias and clickbait incentives. On twitter and reddit, follow people you disagree with.
4. Social media profits off hate and anger because it generates more engagement. Let’s fight back with compassion. ‍Pause. Don’t be so quick to unfollow or publicly argue with someone who posts something you disagree with.‍
5. Be compassionate: Try a private message to ask why they feel that way, with genuine curiosity and a desire to understand.
6. Essentially, you vote with your clicks. If you click on clickbait, you’re creating a financial incentive that perpetuates this existing system. Before you share, fact check. Consider the source. Do that extra Google"
