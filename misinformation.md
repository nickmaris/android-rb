---
title: Privacy is not about Misinformation
published: true
description: Even though misinformation is an issue
tags: privacy
series: Privacy myths
---


Misinformation is an issue and parents and consultants of governmental policy makers need to take action but it is not privacy, so what is it?

Some [forms of misinformation](https://www.opendemocracy.net/en/digitaliberties/how-to-fight-online-misinformation-dont-rely-on-laws-or-algorithms-try-vaccines-instead) are:

1. Polarizing audiences
2. impersonating people (or groups of people) online
3. using emotional language when facts are expected
4. spreading conspiracy theories
5. discrediting opponents and deflecting criticism
6. internet trolling

We analyzed polarization in the previous article extensively as it imposes the worst and most direct danger to democracy among other forms of misinformation. As for the rest, some potentials dangers are:

1. health issues when following bad practices
2. economic fraud

## What you can do

Read only information that helps you take action, action on anything that you really care and ignore everything irrelevant. This will help you avoid sharing misinformation which means that we need to be mindful of the things we do "for fun". For example, some memes we share might be part of a campaign for opinion making.
 
 [First Draft](https://firstdraftnews.org/latest/the-psychology-of-misinformation-how-to-prevent-it) suggests us to:

1. expose people to examples of misinformation, or misinformation techniques, to help them recognize and reject them in the future.
2. nudge people to think about accuracy before sharing. For example, saying that you heard it from a friend or from a facebook group is not enough
3. be aware of the potential for manipulation and evaluate multiple resources before you accept something as true. Try to find official sources before you believe something.

## What is your role

Recommendation engines now weight news with sophisticated algorithms that detect fake news based on e.g. the content of the news or the way they get propagated. However, that cannot completely replace the human eye. If you cannot judge, at least don't press any share/like button.
