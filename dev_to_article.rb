require 'net/http'
require 'uri'
require 'json'
require 'yaml'
require 'date'
require 'git'
require 'logger'

class DevToArticle

def self.dev_to(httpmethod = "Get", params = {}, path, apikey )
  header = {
    'Content-Type': 'application/json',
    'api-key': apikey
  }
  uri = URI.parse("https://dev.to/api/" + path)
  
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  clazz = Object.const_get("Net::HTTP::#{httpmethod}")
  request = clazz.new(uri.request_uri, header)
  request.body = params.to_json
  response = http.request(request)
  if httpmethod != "Get" and not (response.kind_of? Net::HTTPSuccess)
    raise Not200Error.new( httpmethod, params, path, response)
  end
  return response
end


def self.upsert(apikey, username, slug, md)
  params = {
     "article": {
       body_markdown: md
     }
  }
  response = self.dev_to("articles/#{username}/#{slug}", apikey)
  if response.kind_of? Net::HTTPSuccess
    id = JSON.parse(response.body)['id']
    self.dev_to("Put", params, "articles/#{id}", apikey)
    return slug
  end
  response = self.dev_to("Post", params, 'articles', apikey)
  slug = JSON.parse(response.body)['slug']
  return slug
end


def self.publish(apikey, username)
  g = Git.open('.')
  if ENV['CI_COMMIT_REF_NAME'] != 'master'
    exit 0
  end
  puts "Started"
  state = YAML.load_file('state.yml')
  begin
    state.fetch('posts').each { |post|
      filename = post.fetch('filename')
      date = post.fetch('date')
      md = File.read(filename)
      if date == Date.today
        puts "Processing #{filename}"
        puts "slug at state.yml: #{post['slug']}"
        post['slug'] = self.upsert(apikey, username, post['slug'], md)
        puts "slug at dev.to: #{post['slug']}"
      end
    }
  rescue KeyError => e
    new_ex = Exception.new("#{e.message} but got #{e.receiver}")
    new_ex.set_backtrace(e.backtrace)
    raise new_ex
  end
  File.open('state.yml','w') do |h| 
    h.write state.to_yaml
  end
  if g.diff.stats[:total][:files] > 0
    puts g.diff
    g.commit_all('bump')
    g.push('origin', 'HEAD:master')
  end
end

end

class Not200Error < StandardError
  def initialize(msg="dev.to did not return HTTP 200.", exception_type="custom", httpmethod, params, path, response )
    @exception_type = exception_type
    body = '[html skipped]'
    begin
      if not response.body.include?('html')
        body = JSON.parse(response.body)
      end
    rescue
      body = response.body
    end
    @message = {
      response: {
        body: body,
        code: response.code,
        originalError: response.error_type().to_s,
        originalmessage: (' ' + response.message.dump if response.message)
      },
      request: {
        httpmethod: httpmethod,
        path: path,
        params: params
      }
    }
    super(@message)
  end
  def to_s
    JSON.pretty_generate(@message)
  end
end