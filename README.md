Collaborate on gitlab and schedule publishing to dev.to

## Instructions

1. Fork
2. at dev.to set token and then set CI env vars masked DEV_TO_API_KEY, DEV_TO_NAME
3. at gitlab set personal access token with write repository permissions
4. at gitlab set CI env vars masked GITLAB_TOKEN
5. at gitlab set a scheduled pipeline on a daily basis

## How it works

Processes entries in state.yml of master branch daily like the following:

```
- filename: update.md
  date: 2020-11-08
  slug: generated-post-19p6

```

If today is `2020-11-08` then it updates the article with title mentioned in `update.md`.

And if the article does not exist, it creates it and updates `state.yml` with its slug so that it can update it in the future if instructed by `state.yml`.

## The editing pipeline as a...pipeline

Each time you want to write a new article, open a Merge Request, create an entry at `state.yml` and a file.

Then the most important step is to discuss it with other and review it. Markdown and gitlab make it so easy to talk about changes!

Finally, when ready, merge the MR and wait for next day in the scheduled timing.

## Format

Custom variables can be set for each post, located between the triple-dashed lines in your editor. Make sure `published` is true and that there is a unique title:

* title: the title of your article
* published: boolean that determines whether or not your article is published
* description: description area in Twitter cards and open graph cards
* tags: max of four tags, needs to be comma-separated
* canonical_url: link for the canonical version of the content
* cover_image: cover image for post, accepts a URL. The best size is 1000 x 420.
* series: post series name.
