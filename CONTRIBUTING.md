## Posts

Start with typos that are fast to review.

## Tests

Rspec is currently covering only the logic after filesystem and before API calls.

## Code

If there is interest, it is better to cover more edge cases and wrap it to gem than adding more logic.