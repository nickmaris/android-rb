---
title: Privacy is not about Addiction
published: true
description: Even though addiction is an issue
tags: privacy
series: Privacy myths
---

Internet addiction is an issue and parents and consultants of governmental policy makers need to take action but it is not privacy, so what is it?

A kid that empowers all websites to track its behaviour so that they can attract more of its attention, will receive things like more personalized ads and more personalized content. So far, so good but the content might be practically irrelevant as regards the happiness of the kid or even harmful to the point that the kid might end up with depression e.g. because of not enough "likes" in social media. [Social Dilemma](https://scrapsfromtheloft.com/2020/10/03/the-social-dilemma-movie-transcript) says "when we are uncomfortable or lonely or uncertain or afraid, we have a digital pacifier for ourselves".

## Steps that require discipline

For an adult to take control and avoid addiction here are a few steps that require discipline based on [humanetech](https://www.humanetech.com/take-control):

### Set boundaries

1. Clear at least your morning & evenings → Set clear bounded blocks of time without technology like half an hour before bedtime, all devices out.
2. Device-free group activities at home like dinners. Create a shared charging station at home
3. Buy a separate alarm clock → Wake up without getting sucked into your phone first thing in the morning.

### Reduce notifications

1. Find mobile apps that are not a big deal to delete to reduce push notifications and use your browser instead when necessary. And yes the desktop browser for those that are not mobile friendly.
2. Remove email notifications that are not a big deal and again use your browser when You decide to do so.
3. Create an email account for newsletters you care but do not add it in your mobile. You can also forward to this account emails that might be more of a spam than a request to take action.

### Cleanup your mobile homescreen

Replace your mobile homescreen with just a few key apps or notes or photo or contacts of your loved ones. Make it less tempting to distract yourself.

## For the less disciplined

If you can't stop reading "feeds" (any platform that grabs your attention by recommending you things to consume for "free"), at least limit the time you spend on:

[Facebook Netflix Google Microsoft](https://en.m.wikipedia.org/wiki/Big_Tech):

Facebook feed, Snapchat, Instagram
Netflix
Google search, Youtube
Xbox, Linkedin

But also:

Twitter
TikTok
Pinterest
Reddit
add more here...

## For parents to assist their kids

As stated in Socisl Dilemma:

1. "No social media until high school. Personally, I think the age should be 16"
2. And discuss and work out the daily hours spent on a device with your kid

## What else?

Did any of this help you? Have you found something else that worked for you?
