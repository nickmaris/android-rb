---
title: Spark - A service that explains its privacy policy
published: true
description: Choose services that explain their privacy policy
tags: privacy
series: Privacy myths
---

Privacy policies are asking you to take an informed decision, so choose services that explain their privacy policy. [Spark](https://sparkmailapp.com/privacy) is an example of a service that has also an [explanation](https://support.readdle.com/spark/privacy/privacy-explained) of its privacy policy, so let's read how it looked on the 4th of December 2020:

## Buiness model

First of all it makes clear which is the business model:

> Spark’s business model is simple: It’s free for individual users, yet it makes money by offering Premium plans for teams and organizations. 

## Purpose

They talk about purpose limitation

> We don’t use your data for any other purposes. [...] We won’t ask for more data than is needed to provide you with the service.

And they summarize the purpose by saying:

Some of the purposes for processing the data provided by you include:

Providing you with the services
Fraud prevention
Improving our services
Notifying you of any changes in our services

## Honesty and transparency

For example, they say "Your email is safe and we do not use it for profiling or targeting."

## Data retention

They clarify that "We always delete your data once it’s no longer necessary" and the retention period is specified at the section "HOW LONG PERSONAL DATA IS STORED FOR" of the policy.

## Security

Interestingly for a privacy policy, they have a section called "SECURITY MEASURES USED BY US".

Note the Security is not Privacy, it is a precondition for Privacy otherwise what the data you provide to the service are also exposed to unethical hackers.

## Clear instructions on how to get access to your data or request its deletion

"You can either exercise your rights by deleting your account and all information associated with it from your device or by emailing us at dpo@readdle.com." and "Spark is GDPR and CCPA compliant, and you have the right to get access to your data or require its deletion. We are committed to dealing with all privаcy requests promptly and transparently."

## Location

The service is located in Germany, a country that cares about Privacy.

## Review

So overall, the service gives a good impression as most services have eitheir no information, or have information that you need to pay a lawyer to understand it.

I haven't used this service but as you can see in my previous post, we need to read the privacy policy of services and not accept them blindly and our attention span is already limited so invest in services that help you on that by structuring or explaining their privacy policy.

If you don't see the next article in this series called "Privacy myths" yet, follow me at dev.to and I'll post it tomorrow.